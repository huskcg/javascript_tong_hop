// Câu 1
let renderTable = () => {
  for (var i = 1; i <= 10; i++) {
    var string = "";
    for (var j = 1; j <= 10; j++) {
      string += (i - 1) * 10 + j + " ";
    }
    console.log(string);
  }
};
console.log("Câu 1: In ra bảng từ 1-100");
renderTable();
// Câu 2
let findPrimesArray = (array) => {
  let isPrimeNumber = (number) => {
    if (number <= 1) {
      return false;
    }

    for (var i = 2; i < number; i++) {
      if (number % i === 0) {
        return false;
      }
    }

    return true;
  };
  let result_array = [];
  for (var i = 0; i < array.length; i++) {
    if (isPrimeNumber(array[i])) {
      result_array.push(array[i]);
    }
  }
  console.log(result_array);
};
console.log(
  "Câu 2: Cho mảng: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]. Tìm mảng số nguyên tố"
);
findPrimesArray([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);

// Câu 3
let calculateSum = (number) => {
  var s = 0;
  for (var i = 2; i <= number; i++) {
    s += i;
  }
  console.log(s + 2 * number);
};
console.log("Câu 3: Tính S=(2+3+4...+n)+2n với n=6");
calculateSum(6);

// Câu 4
let countDivisor = (number) => {
  var count = 0;
  for (var i = 1; i <= number; i++) {
    if (number % i == 0) {
      count++;
    }
  }
  console.log(count);
};
console.log("Câu 4: Tìm số lượng ước số của một số n, với n=8:");
countDivisor(8);

// Câu 5
let reverseNumber = (number) => {
  var stringNumber = number.toString();
  var result = "";
  for (var i = stringNumber.length - 1; i >= 0; i--) {
    result += stringNumber[i];
  }
  console.log(result);
};
console.log("Câu 5: Đảo ngược 1 số nguyên dương n, với n=13569");
reverseNumber(13569);

// Câu 6
let findX = () => {
  var sum = 0;
  var x = 0;
  for (var i = 1; sum <= 100; i++) {
    sum += i;
    x = i - 1;
  }
  console.log(x);
};
console.log("Câu 6: Tìm x nguyên dương lớn nhất, biết 1+2+3+...+x ≤100");
findX();

// Câu 7
let creatMultiplicationTable = (number) => {
  for (let i = 1; i <= 10; i++) {
    console.log(number + " x " + i + " = " + number * i);
  }
};
console.log("Câu 7: In ra bảng cửu chương n, với n=9");
creatMultiplicationTable(9);

// Câu 8
let playCards = () => {
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  var player1 = [];
  var player2 = [];
  var player3 = [];
  var player4 = [];
  for (var i = 0; i < cards.length; i++) {
    if (i % 4 === 0) {
      player1.push(cards[i]);
    } else if (i % 4 === 1) {
      player2.push(cards[i]);
    } else if (i % 4 === 2) {
      player3.push(cards[i]);
    } else {
      player4.push(cards[i]);
    }
  }
  console.log("Player 1: " + player1);
  console.log("Player 2: " + player2);
  console.log("Player 3: " + player3);
  console.log("Player 4: " + player4);
};
console.log("Câu 8: Viết hàm chia bài");
playCards();

// Câu 9
/**
 * Gà = x
 * Chó = y
 *
 * x + y = 36 => x = 36 - y
 * 2x + 4y = 100 => 2(36-y) + 4y = 100 => 72 - 2y + 4y = 100 => 72 + 2y = 100 => y = (100-72)/2 = 14
 *
 * Tổng quát hoá
 * - Tổng số con là m: => số gà x = m - y
 * - Tổng số chân là n: => số chó y = (n - 2m)/2
 */
let findNumOfChickensAndDogs = (m, n) => {
  var numOfDogs = (n - 2 * m) / 2;
  var numOfChickens = m - numOfDogs;

  if (numOfChickens >= 0 && numOfDogs >= 0) {
    if (numOfChickens % 1 === 0 && numOfDogs % 1 === 0) {
      console.log("Số con gà là: " + numOfChickens);
      console.log("Số con chó là: " + numOfDogs);
    } else {
      console.log("Không tìm thấy số con gà và số con chó"); // Nghiệm lẻ
    }
  } else {
    console.log("Không tồn tại"); // Nghiệm âm
  }
};
console.log("Câu 9: Nếu tổng số con m = 36; số chân n = 100 thì");
findNumOfChickensAndDogs(36, 100);

// Câu 10
/**
 * 1 giờ trôi qua thì kim phút quay dc 360 độ       => 1 phút quay được 360/60 = 6 độ
 * 12 giờ trôi qua thì kim giờ giờ quay dc 360 độ   => 1 giờ quay được 360/12 = 30 độ
 *                                                  => 1 phút quay được 30/60 = 0.5 độ
 *
 * Tính góc giữa 2 kim (ví dụ 4h20p):   angle = |kim phút - kim giờ|
 *                                              |6độ * 20phút - 0.5độ*(4giờ*60phút + 20phút)|
 */

let calcAngle = (hours, minutes) => {
  var angle = Math.abs(6 * minutes - 0.5 * (hours * 60 + minutes));
  return (result = Math.min(360 - angle, angle)); // Lấy góc nhọn
};
console.log("Câu 10: Tính góc độ giữa 2 kim, ví dụ 4 giờ 30 phút");
console.log(calcAngle(4, 30) + " độ");
